﻿using Lesson12.Task1;
using Lesson12.Task2_1;
using Lesson12.Task2_1.Subscriber;

namespace Lesson12
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Task1
            HousingPriceMonitoring monitor = new HousingPriceMonitoring(ShowPrice);
            monitor.HousingPrice();

            //Task2_1
            EmailSubscriber artem = new EmailSubscriber("artem@ya.ru");
            EmailSubscriber pasha = new EmailSubscriber("pasha@ya.ru");
            SMSSubscriber nastya = new SMSSubscriber("89999873274");
            SMSSubscriber alena = new SMSSubscriber("89557659476");

            EventHousingMinPriceMonitoring housingMinPriceMonitoring = new EventHousingMinPriceMonitoring();
            housingMinPriceMonitoring.MinAmountHousingPrices += artem.Notify;
            housingMinPriceMonitoring.MinAmountHousingPrices += pasha.Notify;
            housingMinPriceMonitoring.MinAmountHousingPrices += nastya.Notify;
            housingMinPriceMonitoring.MinAmountHousingPrices += alena.Notify;

            housingMinPriceMonitoring.HousingMinPrice();

            //Task2_2
            EmailSubscriber misha = new EmailSubscriber("misha@ya.ru");
            EmailSubscriber dasha = new EmailSubscriber("dasha@gmail.com");
            SMSSubscriber nadya = new SMSSubscriber("89885268462");
            SMSSubscriber vlad = new SMSSubscriber("89539479375");

            PatternHousingMinPriceMonitoring _housingMinPriceMonitoring = new PatternHousingMinPriceMonitoring();
            _housingMinPriceMonitoring.Subscribe(misha).Subscribe(dasha).Subscribe(vlad).Subscribe(nadya);

            _housingMinPriceMonitoring.HousingMinPrice();
        }
        public static void ShowPrice(int price)
        {
            Console.WriteLine($"Housing price: {price}");
        }
    }
}