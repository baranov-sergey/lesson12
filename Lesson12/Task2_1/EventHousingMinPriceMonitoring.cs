﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson12.Task2_1
{
    /*
        2. Реализовать паттерн наблюдатель в случае если цена на жилье опустилась ниже определенного значения, 
        и сообщить всем кто на это событие подписался. 
        * реализовать как через событие так и через паттерн наблюдатель без события
        https://refactoring.guru/ru/design-patterns/observer 
    */

    internal class EventHousingMinPriceMonitoring
    {
        public event Action<int> MinAmountHousingPrices;

        public void HousingMinPrice()
        {
            Random rnd = new Random();
            int housingPrice = rnd.Next(1000000, 10000000);

            if (housingPrice <= 5000000)
            {
                MinAmountHousingPrices?.Invoke(housingPrice);
            }
            else
            {
                Console.WriteLine("All housing prices exceed 5,000,000");
            }
        }
    }
}
