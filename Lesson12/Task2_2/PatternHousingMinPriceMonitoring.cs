﻿using Lesson12.Task2_1.Subscriber;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson12.Task2_1
{
    /*
        2. Реализовать паттерн наблюдатель в случае если цена на жилье опустилась ниже определенного значения, 
        и сообщить всем кто на это событие подписался. 
        * реализовать как через событие так и через паттерн наблюдатель без события
        https://refactoring.guru/ru/design-patterns/observer 
    */

    internal class PatternHousingMinPriceMonitoring
    {
        private readonly List<ISubscriber> subscribers = new List<ISubscriber>();

        public PatternHousingMinPriceMonitoring Subscribe(ISubscriber subscriber)
        {
            subscribers.Add(subscriber);
            return this;
        }

        public void Unsubscribe(ISubscriber subscriber)
        {
            if (subscribers.Remove(subscriber))
            {
                Console.WriteLine("subscriber was removed");
            }
            else
            {
                Console.WriteLine("No such subscriber");
            }
        }

        public void HousingMinPrice()
        {
            Random rnd = new Random();
            int housingPrice = rnd.Next(1000000, 10000000);

            if (housingPrice <= 5000000)
            {
                foreach (var item in subscribers)
                {
                    item.Notify(housingPrice);
                }
            }
            else
            {
                Console.WriteLine("All housing prices exceed 5,000,000");
            }
        }
    }
}
