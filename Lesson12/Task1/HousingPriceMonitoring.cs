﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson12.Task1
{
    /*
        1. Cоздать класс мониторинга средней цен на жилье,
        цена будет генерироваться с помощью класса рандом и выдавать случайное значение в определенном диапазоне.
        Для того чтобы вывод цены был удобен пользователю в классе мониторинга создать поле делегат,
        обьект которого будет создаваться в классе мониторинга. 
        Пользователь указывает метод для отображения цены в удобном ему формате путем передачи метода
        в конструктор класса мониторинга.
        
        пример создания бьекта монитора.
        
        PriceMonitor monitor = new PriceMonitor(ShowPrice);

        public static void ShowPrice(int price)
        {
        	//your code
        }
     */

    internal class HousingPriceMonitoring
    {
        public delegate void ShowPrice(int price);

        private ShowPrice showPrice = default;

        public HousingPriceMonitoring(ShowPrice showPrice)
        {
            this.showPrice = showPrice;
        }

        public void HousingPrice()
        {
            showPrice(new Random().Next(500000, 1000000));
        }
    }
}
