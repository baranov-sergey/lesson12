﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson12.Task2_1.Subscriber
{
    internal class SMSSubscriber : ISubscriber
    {
        private readonly string phoneNumber;

        public SMSSubscriber(string phoneNumber)
        {
            this.phoneNumber = phoneNumber;
        }

        public void Notify(int housingPrice)
        {
            Console.WriteLine($"SMS to {phoneNumber}: Advantageous offer to purchase an apartment: {housingPrice}");
        }
    }
}
