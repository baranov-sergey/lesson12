﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson12.Task2_1.Subscriber
{
    internal class EmailSubscriber : ISubscriber
    {
        private readonly string email;

        public EmailSubscriber(string email)
        {
            this.email = email;
        }

        public void Notify(int housingPrice)
        {
            Console.WriteLine($"Email to {email}: Advantageous offer to purchase an apartment: {housingPrice}");
        }
    }
}
