﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson12.Task2_1.Subscriber
{
    internal interface ISubscriber
    {
        void Notify(int housingPrice);
    }
}
